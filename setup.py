# https://python-packaging.readthedocs.io
from setuptools import setup

setup(name                 = 'DWMpy',
      version              = '0.1',
      description          = 'A Python wrapper for the stand alone HAWC2 DWM DLL.',
      #url                  = '',
      author               = 'Jaime Liew',
      author_email         = 'jyli@dtu.com',
      license              = 'MIT',
      packages             = ['DWMpy'],
      install_requires     = ['sphinx'],
      zip_safe             = False,
      include_package_date = True,

)
