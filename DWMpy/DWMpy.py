"""
DWM_python.py
====================================
The core module of the DWMpy package
"""
import numpy as np
from numpy.ctypeslib import ndpointer
import ctypes as ct
import _ctypes
import os


    
class DWM_linker(object):
    """Class that links to the DWM DLL."""
    def __init__(self, filename):
        self.dll_filename = str(filename)
        self._original_dir = None


        
    def __enter__(self):
        return self
        
        
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.unload()
        
        # change back to original working directory if it was changed.
        if self._original_dir:
            os.chdir(self._original_dir)
        return False
        
        
        
    def load(self, filename, working_dir=None):
        """
        Loads the DWM DLL using a HAWC2 input file (.htc) as an input.
        
        Parameters
        ----------
        
        filename
            The filename of the HAWC2 input file (.htc) relative to the working directory
        working_dir
            The file path to the working directory. Default is the current working directory.
            
        """
        # Load the DLL into memory
        self.lib = ct.CDLL(self.dll_filename)      
        
        # Run initialisation function  
        f = self.lib.init
        
        f.argtypes = [ct.c_char_p]
        f.restype = None
        
        arg1 = str(filename).encode('utf-8').ljust(255)
        
        if working_dir:
            self._original_dir = os.getcwd()
            os.chdir(working_dir)
        f(arg1)
        
        return self


        
    def unload(self):
        """
        Unloads the DWM DLL.
        """
        _ctypes.FreeLibrary(self.lib._handle)
            
            
    
    def get_wsp(self, t, X, factor=1):
        """
        Returns the windspeed contribution from dynamic wake meandering at a time, t and global position, X.
        
        Parameters
        ----------
        t: float
            Time [s].
        X: list of floats
            position of the form [x, y, z].
        factor: float
            Turbulent scaling factor.
            
        Returns
        -------
        windspeed: list of floats
            wind speed of the form [u, v, w].
        """
        f = self.lib.get_wsp
        
        array_type = ndpointer(shape=3, dtype = ct.c_double, flags='FORTRAN')
        
        f.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), array_type, array_type]
        f.restype = None
        
        arg1 = ct.pointer(ct.c_double(t))
        arg2 = ct.pointer(ct.c_double(factor))
        arg3 = np.array(X, dtype=ct.c_double, order='F')
        arg4 = np.zeros(3, dtype=ct.c_double, order='F')
        
        f(arg1, arg2, arg3, arg4)        
        
        return arg4
        
        
        
    def get_wake_pos(self, t, wake_ind=1):
        """
        Returns the wake center position in the rotorplane in meteorological coordinates (?).
        
        Parameters
        ----------
        t: float
            Time [s].
        wake_ind: int
            Index number of the wake.
        
        Returns
        -------
        position: list of floats
            wake position of the form [x, y].
        """
        f = self.lib.get_wake_pos
        
        array_type = ndpointer(shape=3, dtype = ct.c_double, flags='FORTRAN')
        
        f.argtypes = [ct.POINTER(ct.c_double), ct.POINTER(ct.c_int), array_type]
        f.restype = None
        
        arg1 = ct.pointer(ct.c_double(t))
        arg2 = ct.pointer(ct.c_int(wake_ind))
        arg3 = np.zeros(3, dtype=ct.c_double, order='F')
        
        f(arg1, arg2, arg3)        
        
        return arg3
        
    
    def get_meander(self, meander_time, tint, origin, nrelease=2**13):
        """
        Calculates a new wake meandering path based on the meandering turbulence box.
        
        Parameters
        ----------
        meander_time: float
            The time (in seconds) the wake meanders from emmision until being observed.
        tint: float
            Meandering turbulence intensity.
        origin: list of floats
            Location of the source location of the form [x, y, z] in meteorological coordinates.
        nrelease: int
            Number of wake tracer particles.
            
        
        Returns
        -------
        meander_path: ndarray(shape=(nrelease, 3))
            The meandering path.
        """
        f = self.lib.get_meander
        
        array_type1 = ndpointer(shape=3, dtype = ct.c_double, flags='FORTRAN')
        array_type2 = ndpointer(shape=(nrelease, 3), dtype = ct.c_double, flags='FORTRAN')
        
        f.argtypes = [
            array_type2,                  # meander_path (output)                         
            ct.POINTER(ct.c_int),        # nrelease                                                                
            ct.POINTER(ct.c_double),     # meander time                                                   
            ct.POINTER(ct.c_double),     # tint                                                 
            array_type1,                 # origin                                     
        ]
        
        f.restype = None
        
        arg1 = np.zeros([nrelease, 3], dtype=ct.c_double, order='F')
        arg2 = ct.pointer(ct.c_int(nrelease))
        arg3 = ct.pointer(ct.c_double(meander_time))
        arg4 = ct.pointer(ct.c_double(tint))
        arg5 = np.array(origin, dtype=ct.c_double, order='F')
        
        f(arg1, arg2, arg3, arg4, arg5)        
        
        return arg1
    
    
    def get_axial_induction(self, U, omega, pitch):
        """
        Calculates the axial induction of the rotor as a function of radial position.
        
        Parameters
        ----------
        U: float
            Ambient wind speed [m/s].
        omega: float
            Rotor speed [rad/s].
        pitch: float
            Blade pitch angle [deg]
        
        Returns
        -------
        axial induction: ndarray(shape=(50, 3))
            Wake deficit data. 
            Column 1: radial position, r [m]
            Column 2: axial induction, a [-]
        """
        f = self.lib.get_near_wake_deficit
        
        array_type = ndpointer(shape=(50, 2), dtype = ct.c_double, flags='FORTRAN')
        
        f.argtypes = [
            ct.POINTER(ct.c_double), # U
            ct.POINTER(ct.c_double), # omega
            ct.POINTER(ct.c_double), # pitch (rad) 
            array_type,              # radius, deficit, du/dr 
        ]
        
        f.restype = None
        
        arg1 = ct.pointer(ct.c_double(U))
        arg2 = ct.pointer(ct.c_double(omega))
        arg3 = ct.pointer(ct.c_double(np.deg2rad(pitch)))
        arg4 = np.zeros([50, 2], dtype=ct.c_double, order='F')
        
        f(arg1, arg2, arg3, arg4)          
        
        # convert velocity to axial induction.
        arg4[:, 1] = -arg4[:, 1]/(2*U)
        
        return arg4
        
            
    def get_wake_deficit(self, U, tint, omega, pitch, D):
        """
        Calculates a the wake deficit as a function of radial position.
        
        Parameters
        ----------
        U: float
            Ambient wind speed [m/s].
        tint: float
            Ambient turbulence intensity [-].
        omega: float
            Rotor speed [rad/s].
        pitch: float
            Blade pitch angle [deg]
        D: float
            Downstream distance of observed wake deficit [m]
        
            
        Returns
        -------
        wake_deficit: ndarray(shape=(500, 3))
            Wake deficit data. 
            Column 1: radial position, r [m]
            Column 2: wake deficit, V [m/s]
            Column 3: dV/dr [m/s/m]
        """
        f = self.lib.get_wake_deficit
        
        array_type = ndpointer(shape=(500, 3), dtype = ct.c_double, flags='FORTRAN')
        
        f.argtypes = [
            ct.POINTER(ct.c_double), # U
            ct.POINTER(ct.c_double), # tint
            ct.POINTER(ct.c_double), # omega
            ct.POINTER(ct.c_double), # pitch (rad) 
            ct.POINTER(ct.c_double), # distance
            array_type,              # radius, deficit, du/dr 
        ]
        
        f.restype = None
        
        arg1 = ct.pointer(ct.c_double(U))
        arg2 = ct.pointer(ct.c_double(tint))
        arg3 = ct.pointer(ct.c_double(omega))
        arg4 = ct.pointer(ct.c_double(np.deg2rad(pitch)))
        arg5 = ct.pointer(ct.c_double(D))
        arg6 = np.zeros([500, 3], dtype=ct.c_double, order='F')
        
        f(arg1, arg2, arg3, arg4, arg5, arg6)          
         
        return arg6
    
    def get_wake_deficits(self, U, tint, omega, pitch, Ds):
        """
        Calculates a the wake deficit as a function of radial position.
        Same as get_wake_deficit, but can now take a list of Ds. to be integrated into get_wake_deficit in the future.
        
        Parameters
        ----------
        U: float
            Ambient wind speed [m/s].
        tint: float
            Ambient turbulence intensity [-].
        omega: float
            Rotor speed [rad/s].
        pitch: float
            Blade pitch angle [deg]
        Ds: array
            Downstream distances of observed wake deficit [m]
        
            
        Returns
        -------
        wake_deficit: ndarray(shape=(len(Ds), 500, 3))
            Wake deficit data. The three columns in axis=3 are:\n
            Column 1: radial position, r [m]\n
            Column 2: wake deficit, V [m/s]\n
            Column 3: dV/dr [m/s/m]
        """
        f = self.lib.get_wake_deficits
        
        array_type = ndpointer(shape=(len(Ds), 500, 3), dtype = ct.c_double, flags='FORTRAN')
        array_type2 = ndpointer(shape=(len(Ds)), dtype = ct.c_double, flags='FORTRAN')
        
        f.argtypes = [
            ct.POINTER(ct.c_double), # U
            ct.POINTER(ct.c_double), # tint
            ct.POINTER(ct.c_double), # omega
            ct.POINTER(ct.c_double), # pitch (rad) 
            array_type2,             # distance
            array_type,              # radius, deficit, du/dr 
            ct.POINTER(ct.c_int),    # npos
        ]
        
        f.restype = None
        
        arg1 = ct.pointer(ct.c_double(U))
        arg2 = ct.pointer(ct.c_double(tint))
        arg3 = ct.pointer(ct.c_double(omega))
        arg4 = ct.pointer(ct.c_double(np.deg2rad(pitch)))
        arg5 = np.array(Ds, dtype=ct.c_double, order='F')
        arg6 = np.zeros([len(Ds), 500, 3], dtype=ct.c_double, order='F')
        arg7 = ct.pointer(ct.c_int(len(Ds)))
        f(arg1, arg2, arg3, arg4, arg5, arg6, arg7)          
         
        return arg6
        
    
    
    def get_far_wake_deficits(self, U, tint, R, rs, Udefs, Ds):
        """
        Calculates the far wake deficit as a function of radial position using a user-defined 
        near wake deficit.
        takes a list of Ds.
        
        Parameters
        ----------
        U: float
            Ambient wind speed [m/s].
        tint: float
            Ambient turbulence intensity [-].
        R: float
            Radius of the wake-generating turbine [m].
        rs: array-like
            radial position [m].
        Udefs: array-like
            freestream wind speed of the near deficit at radial positions defined in rs. [m/s]
        Ds: array-like
            Downstream distances of observed wake deficit [m]
        
            
        Returns
        -------
        wake_deficit: ndarray(shape=(len(Ds), 500, 3))
            Wake deficit data. 
            The three columns in axis=3 are:\n
            Column 1: radial position, r [m]\n
            Column 2: wake deficit, V [m/s]\n
            Column 3: dV/dr [m/s/m]
        """
        f = self.lib.get_far_wake_deficits
        
        array_type = ndpointer(shape=(len(Ds), 500, 3), dtype = ct.c_double, flags='FORTRAN')
        array_type2 = ndpointer(shape=(len(Ds)), dtype = ct.c_double, flags='FORTRAN')
        array_type3 = ndpointer(shape=(len(rs)), dtype = ct.c_double, flags='FORTRAN')
        
        f.argtypes = [
            ct.POINTER(ct.c_double), # U
            ct.POINTER(ct.c_double), # tint
            ct.POINTER(ct.c_double), # R
            array_type3,             # rs
            array_type3,             # Udefs 
            array_type2,             # distance
            array_type,              # radius, deficit, du/dr 
            ct.POINTER(ct.c_int),    # npos
            ct.POINTER(ct.c_int),    # nrs
        ]
        
        f.restype = None
        
        arg1 = ct.pointer(ct.c_double(U))
        arg2 = ct.pointer(ct.c_double(tint))
        arg3 = ct.pointer(ct.c_double(R))
        arg4 = np.array(rs, dtype=ct.c_double, order='F')
        arg5 = np.array(Udefs, dtype=ct.c_double, order='F')
        arg6 = np.array(Ds, dtype=ct.c_double, order='F')
        arg7 = np.zeros([len(Ds), 500, 3], dtype=ct.c_double, order='F')
        arg8 = ct.pointer(ct.c_int(len(Ds)))
        arg9 = ct.pointer(ct.c_int(len(rs)))
        
        f(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)          
         
        return arg7
            
            
            
                
    def get_wake_turbulence(self, U, tint, omega, pitch, D):
        """
        Calculates a the wake turbulence scaling factor as a function of radial position.
        
        Parameters
        ----------
        U: float
            Ambient wind speed [m/s].
        tint: float
            Ambient turbulence intensity [-].
        omega: float
            Rotor speed [rad/s].
        pitch: float
            Blade pitch angle [deg]
        D: float
            Downstream distance of observed wake deficit [m]
        
            
        Returns
        -------
        wake_deficit: ndarray(shape=(500, 2))
            Wake deficit data. 
            Column 1: radial position, r [m]
            Column 2: wake turbulence scaling factor, K [-]

        """
        f = self.lib.get_wake_turbulence
        
        array_type = ndpointer(shape=(500, 2), dtype = ct.c_double, flags='FORTRAN')
        
        f.argtypes = [
            ct.POINTER(ct.c_double), # U
            ct.POINTER(ct.c_double), # tint
            ct.POINTER(ct.c_double), # omega
            ct.POINTER(ct.c_double), # pitch (rad) 
            ct.POINTER(ct.c_double), # distance
            array_type,              # radius, turbulent scaling factor
        ]
        
        f.restype = None
        
        arg1 = ct.pointer(ct.c_double(U))
        arg2 = ct.pointer(ct.c_double(tint))
        arg3 = ct.pointer(ct.c_double(omega))
        arg4 = ct.pointer(ct.c_double(np.deg2rad(pitch)))
        arg5 = ct.pointer(ct.c_double(D))
        arg6 = np.zeros([500, 2], dtype=ct.c_double, order='F')
        
        f(arg1, arg2, arg3, arg4, arg5, arg6)          
         
        return arg6

        
if __name__ == '__main__':    
    pass
    

        
    


      



    
