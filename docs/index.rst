

Welcome to DWMpy's documentation!
======================================
.. automodule:: DWMpy.DWMpy
    :members:
    
.. toctree::
   :maxdepth: 2
   :caption: Contents:
