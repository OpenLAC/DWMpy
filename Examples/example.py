from DWMpy import DWM_linker
import matplotlib.pyplot as plt


fn_DLL = 'path/to/standalone_dwm.dll'

# connect DWMpy to standalone DWM DLL
DWM = DWM_linker(fn_DLL) 

# load a turbine using a htc file.
with DWM.load('htc/DTU_10MW_RWT.htc') as sim:
    # Get the radial wake deficit function at 10m/s, TI=0.15, 3D downstream
    deficit = sim.get_wake_deficit(10, 0.15, 1, 0, 3*178)
    
# plot deficit
plt.figure()
plt.xlabel('Radial position [m]')
plt.ylabel('deficit [m/s]')
plt.plot(deficit[:, 0], deficit[:, 1])
plt.show()
