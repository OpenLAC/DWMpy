[![pipeline status](https://gitlab.windenergy.dtu.dk/OpenLAC/DWMpy/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/OpenLAC/DWMpy/commits/master)
## Installation
DWMpy requires a 64bit version of Python 3.x

First, clone this repository:
```
git clone https://gitlab.windenergy.dtu.dk/OpenLAC/dwmpy.git
cd dwmpy
```

Second, install DWMpy using pip (remember the full stop at the end):
```
pip install .
```

## Usage
To use DWMpy, you must have a copy of DWM_standalone.dll (for now, you can only get a copy from jyli@dtu.dk). 
To load an instance of DWM_standalone.dll, you must initialise DWMpy with a HAWC2 input file (.htc file).

DWMpy can be initialised in a Python script as follows:
```
from DWMpy import DWM_linker

DWM = DWM_linker('path/to/dll/DWM_standalone.dll')
with DWM.load('path/to/htcfile.htc') as sim:
    # your DWM-related code
```

## Documentation
A description of the available functions can be found **[here](https://openlac.pages.windenergy.dtu.dk/DWMpy/).**

## Citation

If DWMpy played a role in your research, please cite it. This software can be
cited as:


>    DWMpy. Version 0.1 (2019). Available at https://gitlab.windenergy.dtu.dk/OpenLAC/DWMpy. 

For LaTeX users:



    @misc{DWMpy_2019,
    author = {DTU},
    title = {{DWMpy. Version 0.1}},
    year = {2019},
    publisher = {Gitlab},
    journal = {Gitlab Repository},
    url = {https://gitlab.windenergy.dtu.dk/OpenLAC/DWMpy}
    }